## Installation Express_Generator

```bash
$ npx express-generator
```

## Création de votre application

``` bash
$ express --view=pug nameOfYourApp
```
pug:https://pugjs.org/api/express.html

## Installation des dépendances

```bash
$ cd nameOfYourApp
$ npm install
```

## Lancement de votre application

```bash
$ set DEBUG=nameOfYourApp:* & npm start
```

http://localhost:3000/
