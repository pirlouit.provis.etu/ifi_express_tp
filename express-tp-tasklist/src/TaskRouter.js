var express = require('express');
const {TaskRegistry} = require('./Registry.js');
var router = express.Router();

router.get('/',function(req, resp,){
    resp.status(200).render('task/navigation',{
        list: TaskRegistry.all()
    });
});
router.get('/display/:id',function(req, resp,){
    var id = parseInt(req.params.id);
    console.log("Display: " + id);
    console.log(TaskRegistry.find(id));
    resp.status(200).render('task/navigation',{
        list: [TaskRegistry.find(id)]
    });
});
router.get('/edit/:id',function(req, resp,){
    var id = parseInt(req.params.id);
    console.log("Edit: " + id);
    console.log(TaskRegistry.find(id));
    resp.status(200).render('task/form',{
        task: TaskRegistry.find(id).item
    });
});
router.post('/edit/:id',function(req, resp,){
    var id = parseInt(req.params.id);
    var task = req.body;
    TaskRegistry.delete(id);
    TaskRegistry.insert(task);
    resp.status(200).redirect("/task");
});
router.get('/delete/:id',function(req, resp,){
    var id = parseInt(req.params.id);
    TaskRegistry.delete(id)
    resp.status(200).redirect("/task");
});
router.get('/new',function(req, resp,){
    resp.status(200).render('task/form',{
        task: {}
    });
});
router.post('/new',function(req, resp,){
    var task = req.body;
    TaskRegistry.insert(task);
    resp.status(200).redirect("/task");
});

module.exports = router;