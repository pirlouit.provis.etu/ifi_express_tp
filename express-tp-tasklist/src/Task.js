module.exports.Task = class {

    /**
     * Create a task with a simple name
     * 
     * @param {String} name is the task name
     * @param {String} description is a small text to give more detail about the task
     * @param {Number} start is the date from since the task start
     * @param {Number} duetime is the date where the task sould be complete
     */
    constructor( name, description = "", start = null, duetime = null ){
        this.name = name;
        this.description = description;
        this.start = start;
        this.duetime = duetime;
    }

    /**
     * @returns the task has been started
     */
    isStarted(){

        if( this.start ){
            return this.start < Date.now();
        }

        return false;
    }

    /**
     * @returns if the task is finished
     */
    isFinished(){

        if( this.duetime ){
            return this.duetime < Date.now();
        }

        return false;
    }

    /**
     * @returns {String} a small description of this task
     */
    toString(){

        
        let format = (dateInMillis) => {
            let date = new Date( dateInMillis );
            return `${ date.getDay() }/${ date.getMonth() }/${ date.getFullYear() }`;
        };

        let date = "";
        if( this.start && this.duetime ){
            date = `${ format( this.start ) } -> ${ format( this.duetime ) }`
        } else if( this.start ) {
            date = `${ format( this.start ) } -> no due date`;
        } else if( this.duetime ){
            date = `due to ${ format( this.duetime ) }`;
        }

        return `${ this.name }: ${date}\n${this.description}`;
    }

}