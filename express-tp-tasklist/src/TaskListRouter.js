var express = require('express');
const {TaskListRegistry} = require('./Registry.js');
var router = express.Router();

router.get('/',function(req, resp,){
    resp.status(200).render('list/navigation',{
        list: TaskListRegistry.all()
    });
});
outer.get('/:id',function(req, resp,){
    var id = parseInt(req.params.id);
    resp.status(200).render('list/navigation',{
        list: TaskListRegistry.find(id)
    });
});
router.get('/new',function(req, resp,){
    resp.status(200).render('list/form',{
        tasks: []
    });
});
router.post('/new',function(req, resp,){
    var task = req.body;
    TaskListRegistry.insert(task);
    resp.status(200).redirect("/list");
});
router.get('/:id',function(req, resp,){
    var id = parseInt(req.params.id);
    console.log("Display: " + id);
    console.log(TaskListRegistry.find(id));
    resp.status(200).render('list/view',{
        tasklist: {name:{value:"mylist"}},
        tasks: TaskListRegistry.find(id)
    });
});
router.get('/:id/edit',function(req, resp,){
    var id = parseInt(req.params.id);
    console.log("Edit: " + id);
    console.log(TaskListRegistry.find(id));
    resp.status(200).render('list/form',{
        tasks: TaskListRegistry.find(id)
    });
});
router.post('/:id/edit',function(req, resp,){
    var id = parseInt(req.params.id);
    var taskList = req.body;
    TaskListRegistry.delete(id);
    TaskListRegistry.insert(taskList);
    resp.status(200).redirect("/list");
});
router.get('/:id/delete',function(req, resp,){
    var id = parseInt(req.params.id);
    TaskRegistry.delete(id)
    resp.status(200).redirect("/list");
});


module.exports = router;