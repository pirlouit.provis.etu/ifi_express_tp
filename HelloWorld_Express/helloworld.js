
const express = require('express')
const app = express()

const hostname = '127.0.0.1';
const port = 8080;

app.get('/', function (req, res) {
  res.send('Hello World!\n')
})

app.listen(port, function () {
  console.log(`Server running at http://${hostname}:${port}/`)
})

